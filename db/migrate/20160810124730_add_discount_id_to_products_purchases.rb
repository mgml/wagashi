class AddDiscountIdToProductsPurchases < ActiveRecord::Migration[5.0]
  def change
    add_column :products_purchases, :discount_id, :integer
  end
end
