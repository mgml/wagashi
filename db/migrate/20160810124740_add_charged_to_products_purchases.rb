class AddChargedToProductsPurchases < ActiveRecord::Migration[5.0]
  def change
    add_column :products_purchases, :charged, :decimal, :precision => 8, :scale => 2
  end
end
